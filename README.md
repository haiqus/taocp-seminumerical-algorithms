Notes, solutions to exercises, and code surrounding study of *The Art of Computer Programming: Seminumerical Algorithms* by Donald Knuth.
