require './random_numbers'

class Fixnum
  def _! # factorial
    (1..self).inject(1) {|r,i| r * i}
  end
end

class Float
  def self.chi_squared_pdf(x,k)
    return 0 if x < 0
    num = x.to_f**((k/2.0)-1.0) * Math.exp(-x.to_f/2.0)
    dem = 2.0**(k/2.0) * Math.gamma(k/2.0)
    return num / dem
  end
end

class Array
  def sum
    self.clean
    self.inject(0,:+)
  end

  def product
    self.clean
    self.inject(1,:*)
  end

  def chi_squared_test(distribution)
    distribution.clean
    series = self.zip(distribution)
    series.map! { |chi|
      (chi[0] - chi[1])**2 / chi[1]
    }
    return series.sum
  end

  def kolomogorov_smirnov_test(pdf)
    self.clean
    self.sort!
    n = self.length
    diffs = []
    sqrt_n = Math.sqrt(n)
    (1..n).each { |j|
      diffs << ((j/n) - pdf)
    }
    k_pos = Math.sqrt(n) * diffs.max
    
    diffs = []
    (1..n).each { |j|
      diffs << (pdf - ((j - 1) / n))
    }
    k_neg = sqrt_n * diffs.max

    return [k_pos, k_neg]
  end

  def clean
    self.map! { |n| n.to_f }
  end

end

CHI_SQUARED_PDF = lambda { |x,k| 
  if x < 0.0
    return 0.0
  else
    n     = k / 2.0
    num   = (x ** (n - 1.0)) * Math.exp(-x/2.0)
    denom = (2.0 ** n) * Math.gamma(n)
    return num / denom
  end
}

=begin
# EXERCISES 3.3.1
#
# 3

dist = [4.0, 8.0, 12.0, 16.0, 20.0, 24.0, 20.0, 16.0, 12.0, 8.0, 4.0]
obs = [2,6,10,16,18,32,20,13,16,9,2]

puts obs.chi_squared_test(dist) #=> 7.720833333333332

# 4
probs = [0.08333333333333333, 0.08333333333333333, 0.08333333333333333, 0.08333333333333333, 0.08333333333333333, 0.16666666666666666, 0.08333333333333333, 0.08333333333333333, 0.08333333333333333, 0.08333333333333333, 0.08333333333333333]
dist = probs.map { |p| p * 144 } #=> [12.0, 12.0, 12.0, 12.0, 12.0, 24.0, 12.0, 12.0, 12.0, 12.0, 12.0]
obs = [4,10,10,13,20,18,18,11,13,14,13]

puts obs.chi_squared_test(dist) #=> 16.499999999999996
=end
# 10
puts PairOfDice.roll(144)
