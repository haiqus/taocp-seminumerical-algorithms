Array.prototype.sum = function() {
  sum = 0;
  for (i = 0; i < this.length; i++) {
    sum += this[i];
  };
  return sum;
};

Array.prototype.product = function() {
  product = 1;
  for (i = 0; i < this.length; i++) {
    product *= this[i];
  };
  return product;
};

Math.gamma = function(n) {
  return Math.factorial(n - 1)
};

Math.factorial = function(n) {
  var result;
  if (n < 2) {
    result = 1;
  } else {
    result = n * Math.factorial(n - 1);
  }
  return result;
};

function ChiSquaredPDF(x,k) {
  var result;
  if ( x == 0 ) {
    result = 0;
  } else {
    numer_1 = x ** (( k / 2 ) - 1 );
    numer_2 = Math.exp(-x / 2);
    denom_1 = 2 ** (k / 2);
    denom_2 = Math.gamma(k / 2);
    result = ((numer_1 * numer_2 ) / ( denom_1 * denom_2 ));
  }
  return result;
};

function ChiSquaredTest(obs,dist) {


};
