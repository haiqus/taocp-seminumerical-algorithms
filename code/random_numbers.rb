=begin
LDA  A  rA  <- a
MUL  X  rAX <- (rA) * X
SLAX 5  rA  <- rAX mod w
ADD  C  rA  <- (rA + c) mod w
=end

class LCM
  DEFAULT_WORD_SIZE = 2**5

  attr_accessor :multiplier, :seed, :increment, :modulus, :n
  attr_reader :seq

  def initialize(options={})
    self.multiplier = options[:multiplier] ||= 2
    self.seed = options[:seed] ||= 1
    self.increment = options[:increment] ||= 1
    self.modulus = options[:modulus] ||= DEFAULT_WORD_SIZE
    self.n = options[:n] ||= 10
    @seq = nil
  end

  def generate
    seq = []
    n = self.n
    x = self.seed
    begin
      x = LCM.calc_next_random(self.multiplier, x, self.increment, self.modulus)
      seq.push x
      n -= 1
    end while n > 0
    @seq = seq
  end

  def self.calc_next_random(a,x,c,m)
    a = (a*x).modulo(m)
    (a + c).modulo(m)
  end

end


class PairOfDice

  FACES = [1,2,3,4,5,6]

  def self.roll(n)

    long_sequence = LCM.new(:seed => 0, :multiplier => 3141592653, :increment => 2718281829, :modulus => 2**35, :n => n*2)
    long_sequence.generate
    seq = long_sequence.seq
    rolls = []

    n.times do
      die1 = PairOfDice.extract(seq.shift)
      die2 = PairOfDice.extract(seq.shift)
      rolls << die1 + die2
    end
   
    return rolls

  end

  def self.extract(f)
    begin
      f = f / 10
    end while (9 < f)
    return f
  end

end 

def dice_bins(arr)
  dice_bins = {2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0}

  for n in arr
    dice_bins[n] += 1
  end

  return dice_bins
end
